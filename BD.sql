-- Crear la base de datos si a�n no existe
CREATE DATABASE  PruebaTecnicaDB;

-- Usar la base de datos reci�n creada
USE PruebaTecnicaDB;

-- Crear la tabla "clientes" con los campos en notaci�n camello


CREATE TABLE [dbo].[clientes](
	[Identificacion] [int] IDENTITY(1,1) NOT NULL,
	[PrimerNombre] [varchar](255) NULL,
	[PrimerApellido] [varchar](255) NULL,
	[Edad] [int] NULL,
	[FechaCreacion] [datetime] NULL,
 CONSTRAINT [PK__clientes__C196DEC6B82A015F] PRIMARY KEY CLUSTERED 
(
	[Identificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO