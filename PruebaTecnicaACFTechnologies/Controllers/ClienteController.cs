﻿using API.Models;
using API.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : Controller
    {
        private readonly IApiRepository _repo;

        public ClienteController(IApiRepository repo)
        {
            _repo = repo;
            
        }



        [HttpGet]
        public async Task<IActionResult> GetClientes()
        {
            var clientes = await _repo.GetClientesAsync();
            return Ok(clientes);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var producto = await _repo.GetClientesByIdAsync(id);

            return Ok(producto);
        }
        [HttpPost]
        public async Task<IActionResult> CrearCliente(Cliente cliente)
        {
            var clientetocreate = new Cliente();
            clientetocreate.PrimerNombre = cliente.PrimerNombre;
            clientetocreate.PrimerApellido = cliente.PrimerApellido;
            clientetocreate.Edad = cliente.Edad;
            clientetocreate.FechaCreacion = DateTime.Now;

            _repo.Add(clientetocreate);
            if(await _repo.SaveAll())
                return Ok(clientetocreate);

            return BadRequest();

        }

        [HttpPut]
        public async Task<IActionResult> ActualizarCliente( Cliente cliente)
        {
         
          var clientetoupdate = await _repo.GetClientesByIdAsync(cliente.Identificacion);

            if (clientetoupdate == null)
                return BadRequest();

            clientetoupdate.PrimerNombre = cliente.PrimerNombre;
            clientetoupdate.PrimerApellido = cliente.PrimerApellido;
            clientetoupdate.Edad = cliente.Edad;
            if(!await _repo.SaveAll()) 
                return NoContent();

            return Ok(clientetoupdate);

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> EliminarCliente(int id)
        {
            var cliente = await _repo.GetClientesByIdAsync(id);

            if(cliente == null)
                return NotFound("Cliente no encontrado");

            _repo.Delete(cliente);
            if(!await _repo.SaveAll() ) 
                return BadRequest();

            return Ok("Cliente Borrado");
        }
    }
}
