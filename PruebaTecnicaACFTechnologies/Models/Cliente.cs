﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class Cliente
    {
        [Key]
        public int Identificacion { get; set; }
        public string PrimerNombre { get; set; }
        public string PrimerApellido { get; set; }
        public int Edad { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
