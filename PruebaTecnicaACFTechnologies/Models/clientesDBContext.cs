﻿using Microsoft.EntityFrameworkCore;

namespace API.Models
{
    public class clientesDBContext: DbContext
    {
        public clientesDBContext(DbContextOptions<clientesDBContext> options) : base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
    }
}
