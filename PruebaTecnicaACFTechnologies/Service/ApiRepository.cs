﻿using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Service
{
    public class ApiRepository : IApiRepository
    {

        private readonly clientesDBContext _context;
        public ApiRepository(clientesDBContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<IEnumerable<Cliente>> GetClientesAsync()
        {
            var cliente = await _context.Clientes.ToListAsync();
            return cliente;
        }

        public async Task<Cliente> GetClientesByIdAsync(int id)
        {
            var cliente = await _context.Clientes.FirstOrDefaultAsync(u => u.Identificacion == id);
            return cliente;
        }

        public async Task<Cliente> GetClientesByNombreAsync(string nombre)
        {
            var cliente = await _context.Clientes.FirstOrDefaultAsync(u => u.PrimerNombre == nombre);
            return cliente;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }
    }
}
