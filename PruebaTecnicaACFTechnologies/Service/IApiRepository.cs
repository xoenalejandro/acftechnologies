﻿using API.Models;

namespace API.Service
{
    public interface IApiRepository
    {

        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<Cliente>> GetClientesAsync();
        Task<Cliente> GetClientesByIdAsync(int id);
        Task<Cliente> GetClientesByNombreAsync(string nombre);
       


    }
}
