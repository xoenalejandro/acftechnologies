﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WEB.Models;
using WEB.Service;

namespace WEB.Controllers
{
    public class HomeController : Controller
    {


        private IServicioAPI _servicioApi;

        public HomeController(IServicioAPI servicioApi)
        {
            _servicioApi = servicioApi;
        }

        public async Task<IActionResult> Index()
        {
            List<Cliente> lista = await _servicioApi.Lista();
            return View(lista);
        }


        public async Task<IActionResult> Cliente(int identificacion)
        {

            Cliente modelo_cliente = new Cliente();

            ViewBag.Accion = "Nuevo Cliente";

            if (identificacion != 0)
            {

                ViewBag.Accion = "Editar Cliente";
                modelo_cliente = await _servicioApi.Obtener(identificacion);
            }

            return View(modelo_cliente);
        }

        [HttpPost]
        public async Task<IActionResult> GuardarCambios(Cliente ob_cliente)
        {

            bool respuesta;

            if (ob_cliente.Identificacion == 0)
            {
                respuesta = await _servicioApi.Guardar(ob_cliente);
            }
            else
            {
                respuesta = await _servicioApi.Editar(ob_cliente);
            }


            if (respuesta)
                return RedirectToAction("Index");
            else
                return NoContent();

        }
        [HttpGet]
        public async Task<IActionResult> Eliminar(int identificacion)
        {

            var respuesta = await _servicioApi.Eliminar(identificacion);

            if (respuesta)
                return RedirectToAction("Index");
            else
                return NoContent();
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}