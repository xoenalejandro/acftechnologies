﻿namespace WEB.Models
{
    public class Cliente
    {
        public int Identificacion { get; set; }
        public string PrimerNombre { get; set; }
        public string PrimerApellido { get; set; }
        public int Edad { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
