﻿using WEB.Models;

namespace WEB.Service
{
    public interface IServicioAPI
    {
        Task<List<Cliente>> Lista();
        Task<Cliente> Obtener(int Identificador);

        Task<bool> Guardar(Cliente objeto);

        Task<bool> Editar(Cliente objeto);

        Task<bool> Eliminar(int Identificador);
    }
}
