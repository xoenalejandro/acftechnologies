﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using WEB.Models;

namespace WEB.Service
{
    public class ServicioAPI : IServicioAPI
    {
        private static string _baseUrl;

        public ServicioAPI()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            _baseUrl = builder.GetSection("ApiSetting:baseUrl").Value;
        }
        public async Task<List<Cliente>> Lista()
        {
            List<Cliente> lista = new List<Cliente>();

            


            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseUrl);
            var response = await cliente.GetAsync("api/Cliente");

            if (response.IsSuccessStatusCode)
            {

                var json_respuesta = await response.Content.ReadAsStringAsync();
                lista = JsonConvert.DeserializeObject<List<Cliente>>(json_respuesta);
                //lista = resultado.;
            }

            return lista;
        }
        public async Task<bool> Editar(Cliente objeto)
        {
            bool respuesta = false;
                       

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseUrl);
            
            var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");

            var response = await cliente.PutAsync("api/Cliente", content);

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }

        public async Task<bool> Eliminar(int Identificador)
        {
            bool respuesta = false;

            


            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseUrl);
           


            var response = await cliente.DeleteAsync($"api/Cliente/{Identificador}");

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }

        public async Task<bool> Guardar(Cliente objeto)
        {
            bool respuesta = false;



            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseUrl);
            var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");

            var response = await cliente.PostAsync("api/Cliente", content);

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }

       

        public async Task<Cliente> Obtener(int Identificador)
        {
            Cliente objeto = new Cliente();

            


            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseUrl);
            
            var response = await cliente.GetAsync($"api/Cliente/{Identificador}");

            if (response.IsSuccessStatusCode)
            {

                var json_respuesta = await response.Content.ReadAsStringAsync();
                objeto = JsonConvert.DeserializeObject<Cliente>(json_respuesta);
                
            }

            return objeto;
        }
    }
}
